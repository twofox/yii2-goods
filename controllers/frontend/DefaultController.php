<?php

namespace twofox\goods\controllers\frontend;

use yii\web\Controller;
use twofox\goods\models\Goods;
use yii\web\NotFoundHttpException;
use twofox\goods\Module;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * View goods of module.
 * 
 * @author Belosludcev Vasilij http://mihaly4.ru
 * @since 1.0.0
 */
class DefaultController extends Controller
{
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['get'],
                'view' => ['get'],
            ]
        ];

        return $behaviors;
    }    
    
    /**
     * View of page by alias.
     * @param string $page Alias of page.
     * @see Page::$alias
     */
    public function actionIndex()
    {
        $query = Goods::find()->where(['published' => Goods::PUBLISHED_YES]);
        $dataProviderGoods = new ActiveDataProvider(['query' => $query]);
        return $this->render('index', [
            'dataProvider' => $dataProviderGoods,
        ]);
    }   
    
    /**
     * View of page by alias.
     * @param string $page Alias of page.
     * @see Page::$alias
     */
    public function actionView($slug)
    {
        $model = $this->findModel($slug);
        return $this->render('view', ['model' => $model]);
    }
    
    /**
     * Finds the Page model based on alias value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $alias
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($alias)
    {
        $model = Goods::find()->where([
            'slug' => $alias, 
            'published' => Goods::PUBLISHED_YES,
        ])->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Module::t('PAGE_NOT_FOUND'));
    }
    
}

