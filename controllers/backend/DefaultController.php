<?php

namespace twofox\goods\controllers\backend;

use Yii;
use twofox\goods\models\Goods;
use twofox\goods\models\GoodsSearch;
use twofox\goods\models\GoodsLocalization;
use twofox\goods\models\GoodsLocalizationSearch;
use twofox\goods\models\GoodsProducts;
use twofox\goods\models\GoodsProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use twofox\goods\Module;
use vova07\imperavi\actions\GetAction as ImperaviGetAction;
use vova07\imperavi\actions\UploadAction as ImperaviUploadAction;
use zxbodya\yii2\galleryManager\GalleryManagerAction;
use yii\helpers\ArrayHelper;
use backend\modules\user\models\User;

/**
 * ManagerController implements the CRUD actions for Page model.
 * @author Belosludcev Vasilij <bupy765@gmail.com>
 * @since 1.0.0
 */
class DefaultController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return ['verbs' => ['class' => VerbFilter::className(), 'actions' => ['delete' => ['post'], ], ], ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        $module = Yii::$app -> getModule('goods');

        $actions = [];

        // add images that have already been uploaded
        if ($module -> addImage) {
            $actions['images-get'] = ['class' => ImperaviGetAction::className(), 'url' => Yii::getAlias($module -> urlToImages), 'path' => Yii::getAlias($module -> pathToImages), 'type' => ImperaviGetAction::TYPE_IMAGES, ];
        }
        // upload image
        if ($module -> uploadImage) {
            $actions['image-upload'] = ['class' => ImperaviUploadAction::className(), 'url' => Yii::getAlias($module -> urlToImages), 'path' => Yii::getAlias($module -> pathToImages), ];
        }
        // add files that have already been uploaded
        if ($module -> addFile) {
            $actions['files-get'] = ['class' => ImperaviGetAction::className(), 'url' => Yii::getAlias($module -> urlToFiles), 'path' => Yii::getAlias($module -> pathToFiles), 'type' => ImperaviGetAction::TYPE_FILES, ];
        }
        // upload file
        if ($module -> uploadFile) {
            $actions['file-upload'] = ['class' => ImperaviUploadAction::className(), 'url' => Yii::getAlias($module -> urlToFiles), 'path' => Yii::getAlias($module -> pathToFiles), ];
        }

        if ($module -> useGallery) {
            $actions['galleryApi'] = ['class' => GalleryManagerAction::className()];
            $actions['galleryApi']['types'][Yii::$app -> getModule('goods') -> tableName] = Goods::className();
            $actions['galleryApi']['types'][Yii::$app -> getModule('goods') -> tableNameLocalization] = GoodsLocalization::className();
            $actions['galleryApi']['types'][Yii::$app -> getModule('goods') -> tableNameProducts] = GoodsProducts::className();
        }

        return $actions;
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new GoodsSearch();
        $dataProvider = $searchModel -> search(Yii::$app -> request -> queryParams);

        return $this -> render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, ]);
    }

    public function actionView($id) {
        return $this -> render('view', ['model' => $this -> findModel($id), ]);
    }

    public function actionViewProduct($id) {
        return $this -> render('view-product', ['model' => $this -> findModel($id, '\twofox\goods\models\GoodsProducts'), ]);
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionLocalization() {
        $searchModel = new GoodsLocalizationSearch();
        $dataProvider = $searchModel -> search(Yii::$app -> request -> queryParams);

        return $this -> render('localization', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, ]);
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionProducts() {
        $searchModel = new GoodsProductsSearch();
        $dataProvider = $searchModel -> search(Yii::$app -> request -> queryParams);

        return $this -> render('products', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @return mixed
     */
    public function actionCreate() {
        return $this -> actionUpdate(null);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'update' page.
     * @param integer|null $id
     * @return mixed
     */
    public function actionUpdate($id = null, $class = '', $suffix = '') {

        $class = '\\twofox\\goods\\models\\' . (empty($class) ? 'Goods' : $class);

        if ($id === null) {
            $model = new $class;
        } else {
            $model = $this -> findModel($id, $class::className());
        }

        if ($model -> load(Yii::$app -> request -> post())) {

            foreach (Yii::$app->request->post('PostTranslation', []) as $language => $data) {
                foreach ($data as $attribute => $translation)
                    $model -> translate($language) -> $attribute = $translation;

                $model -> translate($language) -> class = $model -> className();
                $model -> translate($language) -> post_id = ($id == null ? 1 : $model -> id);
            }
            
            $model -> validate();

            if ($model -> save(false)) {
                Yii::$app -> session -> setFlash('success', Module::t('SAVE_SUCCESS'));
                return $this -> redirect(['update' . $suffix, 'id' => $model -> id]);
            } else {
                Yii::$app -> session -> setFlash('error', Module::t('Create error. {0}', $model -> formatErrors()));
                return $this -> refresh();
            }
        }

        $module = Yii::$app -> getModule('goods');

        return $this -> render($id === null ? 'create' : 'update', ['model' => $model, 'module' => $module, ]);
    }

    public function actionProductList($goods = null, $currency = null, $q = null, $adv = null) {
        \Yii::$app -> response -> format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => []];
        $query = Goods::getAllProducts($goods, $currency, $q, $adv, Yii::$app -> request -> get('new', 1));
        foreach ($query as $key => $value) {
            $out['results'][] = ['id' => $value['id'], 'title' => $value['title'] . ' (' . $value['price'] . ' ' . $value['currency'] . ')' . ($value['remove'] == 1 ? ' - removed' : '')];
        }
        return $out;
    }

    public function actionCreateLocalization() {
        return $this -> actionUpdate(null, 'GoodsLocalization', '-localization');
    }

    public function actionUpdateLocalization($id = null) {
        return $this -> actionUpdate($id, 'GoodsLocalization', '-localization');
    }

    public function actionCreateProduct() {
        return $this -> actionUpdate(null, 'GoodsProducts', '-product');
    }

    public function actionUpdateProduct($id = null) {
        return $this -> actionUpdate($id, 'GoodsProducts', '-product');
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $class = 'Goods') {
        $class = 'twofox\\goods\\models\\' . (empty($class) ? 'Goods' : $class);
        $data = $this -> findModel($id, $class);
        $data -> remove = 1;
        if ($data -> save())
            Yii::$app -> session -> setFlash('success', Yii::t('action', 'DELETE SUCCESS'));
        $redirect = Yii::$app -> request -> get('redirect', 'index');
        return $this -> redirect([$redirect]);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteLocalization($id) {
        return $this -> actionDelete($id, 'GoodsLocalization');
    }

    public function actionDeleteProduct($id) {
        return $this -> actionDelete($id, 'GoodsProducts');
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $modelclass = '\\twofox\\goods\\models\\Goods') {
        if (($model = $modelclass::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Module::t('PAGE_NOT_FOUND'));
    }

}
