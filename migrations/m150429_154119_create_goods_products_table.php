<?php

use yii\db\Schema;
use yii\db\Migration;

class m150429_154119_create_goods_products_table extends Migration
{
    
    private $_tableName;
    
    public function init()
    {
        parent::init();
        $this->_tableName = Yii::$app->getModule('goods')->tableNameProducts;
    }
    
    public function up()
    {
        $this->createTable(
            $this->_tableName,
            [
                'id' => Schema::TYPE_PK,
                'title' => Schema::TYPE_STRING . ' NOT NULL',
                'slug' => Schema::TYPE_STRING . ' NOT NULL',
                'published' => Schema::TYPE_BOOLEAN . ' DEFAULT 1',
                'annonce' => Schema::TYPE_TEXT,
                'content' => Schema::TYPE_TEXT,
                'meta_title' => Schema::TYPE_STRING,
                'meta_keywords' => Schema::TYPE_STRING . '(200)',
                'meta_description' => Schema::TYPE_STRING . '(160)',
                'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT "0000-00-00 00:00:00"',
                'updated_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT "0000-00-00 00:00:00"',
            ]
        );
        $this->createIndex('slug', $this->_tableName, ['slug'], true);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
