<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Create {{%page}} table.
 * 
 * Create new migration to your application: ```./yii migrate/create twofox_goods_create_page_table```.
 * Extended from this class your new migration class, change tableName to module configurations if need, run migrate.
 * 
 * @example
 * class m140429_155666_twofox_goods_create_page_table extends \twofox\goods\migrations\m150429_155009_create_page_table {
 * }
 * 
 * @author Vasilij Belosludcev http://mihaly4.ru
 * @since 1.0.0
 */
class m150429_155019_create_page_table extends Migration
{
    
    private $_tableName;
    
    public function init()
    {
        parent::init();
        $this->_tableName = Yii::$app->getModule('goods')->tableName;
    }
    
    public function up()
    {
        $this->createTable(
            $this->_tableName,
            [
                'id' => Schema::TYPE_PK,
                'title' => Schema::TYPE_STRING . ' NOT NULL',
                'slug' => Schema::TYPE_STRING . ' NOT NULL',
                'published' => Schema::TYPE_BOOLEAN . ' DEFAULT 1',
                'annonce' => Schema::TYPE_TEXT,
                'content' => Schema::TYPE_TEXT,
                'localization' => Schema::TYPE_INTEGER,
                'meta_title' => Schema::TYPE_STRING,
                'meta_keywords' => Schema::TYPE_STRING . '(200)',
                'meta_description' => Schema::TYPE_STRING . '(160)',
                'created_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT "0000-00-00 00:00:00"',
                'updated_at' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT "0000-00-00 00:00:00"',
            ]
        );
        $this->createIndex('slug', $this->_tableName, ['slug'], true);
    }

    public function down()
    {
        $this->dropTable($this->_tableName);
    }
}
