<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use twofox\goods\Module;
use vova07\imperavi\Widget as Imperavi;
use yii\helpers\Url;

$attributeLabels = $model->attributeLabels();

/* @var $this yii\web\View */
/* @var $model twofox\goods\models\Page */
/* @var $form yii\widgets\ActiveForm */

$form = ActiveForm::begin();
$settings = [
    'lang' => Yii::$app->language,
    'minHeight' => 200,
    'plugins' => [
        'fullscreen',
    ],
];
if ($module->addImage || $module->uploadImage) {
    $settings['plugins'][] = 'imagemanager';
}
if ($module->addImage) {
    $settings['imageManagerJson'] = Url::to(['images-get']);
}
if ($module->uploadImage) {
    $settings['imageUpload'] = Url::to(['image-upload']);
}
if ($module->addFile || $module->uploadFile) {
    $settings['plugins'][] = 'filemanager';
}
if ($module->addFile) {
    $settings['fileManagerJson'] = Url::to(['files-get']);
}
if ($module->uploadFile) {
    $settings['fileUpload'] = Url::to(['file-upload']);
}
?>
        <ul class="nav nav-tabs lang">
           <?php foreach (Yii::$app->urlManager->languages as $key): ?>
                <li<?= Yii::$app -> language == $key ? ' class="active"' : '' ?>>
                    <a id="<?= $key ?>-tab" aria-expanded="true" aria-controls="<?= $key ?>" data-toggle="tab" role="tab" href="#<?= $key ?>"><?= strtoupper($key) ?></a>
                </li>
            <?php endforeach; ?>
        </ul>

       <div class="tabbable"> 
                        <div class="tab-content">                          

                            <?php foreach (Yii::$app->urlManager->languages as $key): ?>
                            <div aria-labelledby="<?= $key ?>-tab" id="<?= $key ?>" class="tab-pane fade<?= Yii::$app -> language == $key ? ' active in' : '' ?>" role="tabpanel">
     
                                <?php if($key==Module::sourceLanguage()){ ?>
                                     <?= $form -> field($model, "title")->textInput(['maxlength' => 255]) ?>   
                                <?php } else { ?>    
                                     <?= $form -> field($model -> translate($key), "[$key]title")->textInput(['maxlength' => 255]) -> label($attributeLabels['title']) ?>
                                <?php } ?>
                                
                                <?php if($key==Module::sourceLanguage()){ ?>
                                     <?= $form -> field($model, "content")->widget(Imperavi::className(), ['settings' => $settings]) ?>   
                                <?php } else { ?>    
                                     <?= $form->field($model -> translate($key), "[$key]content")->widget(Imperavi::className(), ['settings' => $settings]) -> label($attributeLabels['content']); ?>
                                <?php } ?>
                                
                                <?php if($key==Module::sourceLanguage()){ ?>
                                     <?= $form -> field($model, "meta_title")->textInput(['maxlength' => 255]) ?>   
                                <?php } else { ?>    
                                     <?= $form -> field($model -> translate($key), "[$key]meta_title")->textInput(['maxlength' => 255]) -> label($attributeLabels['meta_title']) ?>
                                <?php } ?>
                                
                                <?php if($key==Module::sourceLanguage()){ ?>
                                     <?= $form -> field($model, "meta_keywords")->textInput(['maxlength' => 255]) ?>   
                                <?php } else { ?>    
                                     <?= $form -> field($model -> translate($key), "[$key]meta_keywords")->textInput(['maxlength' => 255]) -> label($attributeLabels['meta_keywords']) ?>
                                <?php } ?>
                                
                                <?php if($key==Module::sourceLanguage()){ ?>
                                     <?= $form -> field($model, "meta_description")->textInput(['maxlength' => 255]) ?>   
                                <?php } else { ?>    
                                     <?= $form -> field($model -> translate($key), "[$key]meta_description")->textInput(['maxlength' => 255]) -> label($attributeLabels['meta_description']) ?>
                                <?php } ?>
</div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                    
                    
<?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>
<?= $form->field($model, 'published')->checkbox()->dropDownList($model->getStatusArray()) ?>
<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Module::t('CREATE') : Module::t('UPDATE'), [
        'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
    ]); ?>
</div>
<?php ActiveForm::end(); ?>
