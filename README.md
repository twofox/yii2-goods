yii2-goods
==========

[![Latest Stable Version](https://poser.pugx.org/twofox/yii2-goods/v/stable)](https://packagist.org/packages/twofox/yii2-page)
[![Total Downloads](https://poser.pugx.org/twofox/yii2-goods/downloads)](https://packagist.org/packages/twofox/yii2-goods)
[![Latest Unstable Version](https://poser.pugx.org/twofox/yii2-goods/v/unstable)](https://packagist.org/packages/twofox/yii2-goods)
[![License](https://poser.pugx.org/twofox/yii2-goods/license)](https://packagist.org/packages/twofox/yii2-goods)

Module implements CRUD with static goods with uses Imperavi Redactor.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist twofox/yii2-goods "*"
```

or add

```
"twofox/yii2-goods": "*"
```

to the require section of your `composer.json` file.


Installation
------------

**Add module to your config file:**

```php
'modules' => [
    ...

    'goods' => [
        'class' => 'twofox\goods\Module',
    ],
]
```

By default module uses table name '{{%page}}'. If in your database this table is 
exist - change it adding to configuration of module new table name:

```php
'modules' => [
    ...

    'goods' => [
        'class' => 'twofox\goods\Module',
        'tableName' => '{{%your_table_name}}',
    ],
]
```

**Run migration**

```php
./yii migrate/up --migrationPath=@twofox/goods/migrations
```

**Install Translateable Behavior**
```
https://github.com/creocoder/yii2-translateable
```

Usage
-----

In module two controllers: ```default``` and ```manager```.

**manager** need for control the goods out of the control panel. You need 
protect it controller via ```controllerMap``` or override it for add behavior with ```AccessControl```.

Example:

```php
'modules' => [
    ...

    'goods' => [
        ...

        'controllerMap' => [
            'manager' => [
                'class' => 'twofox\goods\controllers\ManagerController',
                'as access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['admin'],
                        ],
                    ],
                ],
            ],
        ],
    ],
],
```

**default** for display of goods to site. You need add url rules to
file of config for getting content via aliases goods.

Example:

```php
'urlManager' => [
    'rules' => [
        ...

        'goods/<page:[\w-]+>' => 'goods/default/index',
    ],
],
```

You can upload and add files/images via Imperavi Redactor, if enable it:

```php
'modules' => [
    ...
    
    'goods' => [
        ...

        'pathToImages' => '@webroot/images',
        'urlToImages' => '@web/images',
        'pathToFiles' => '@webroot/files',
        'urlToFiles' => '@web/files',
        'uploadImage' => true,
        'uploadFile' => true,
        'addImage' => true,
        'addFile' => true,
    ],
],
```

License
-------

yii2-goods is released under the BSD 3-Clause License.
