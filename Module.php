<?php

namespace twofox\goods;

use Yii;
use yii\base\InvalidConfigException;

/**
 * Module implements CRUD with static goods.
 * 
 * @author Vasilij Belosludcev http://mihaly4.ru
 * @since 1.0.0
 */
class Module extends \yii\base\Module
{
    
    /**
     * @var string Table name of model \twofox\goods\models\Page.
     * @see \yii\db\ActiveRecord::tableName()
     */
    public $tableName = '{{%goods}}';    
    public $useGallery = false;
    
    
    /**
     * @var string Table name of model \twofox\goods\models\Page.
     * @see \yii\db\ActiveRecord::tableName()
     */
    public $tableNameLocalization = '{{%goods-localization}}';
    public $tableNameProducts = '{{%goods-products}}';
    
    /**
     * @var boolean Enable ability add images via Imperavi Redactor? If this property is 'true', you must be
     * set property '$pathToImages' and '$urlToImages'.
     */
    public $addImage = false;
    
    /**
     * @var boolean Enable ability upload images via Imperavi Redactor? If this property is 'true', you must be
     * set property '$pathToImages', '$urlToImages' and '$imageUploadAction'.
     */
    public $uploadImage = false;
    
    /**
     * @var string Alias of absolute path to directory with images for upload images via Imperavi Redactor. If not 
     * set, then this ability not used.
     * @example '@webroot/uploads/images'
     * @see \vova07\imperavi\actions\GetAction::$path
     */
    public $pathToImages = '@webroot/images';
    
    /**
     * @var string Alias of URL to directory with images for get images uploaded via Imperavi Redactor. If not set, 
     * then this ability not used.
     * @example '@web/uploads/images'
     * @see \vova07\imperavi\actions\GetAction::$url
     */
    public $urlToImages = '@web/images';
    
    /**
     * @var boolean Enable ability add files via Imperavi Redactor? If this property is 'true', you must be
     * set property '$pathToFiles' and '$urlToFiles'.
     */
    public $addFile = false;
    
    /**
     * @var boolean Enable ability upload files via Imperavi Redactor? If this property is 'true', you must be
     * set property '$pathToFiles', '$urlToFiles' and '$fileUploadAction'.
     */
    public $uploadFile = false;
    
    /**
     * @var string Alias of absolute path to directory with files for upload files via Imperavi Redactor. If not set, 
     * then this ability not used.
     * @example '@webroot/uploads/files'
     * @see \vova07\imperavi\actions\GetAction::$path
     */
    public $pathToFiles = '@webroot/files';
    
    /**
     * @var string Alias of URL to directory with files for uploaded files via Imperavi Redactor. If not set, then 
     * this ability not used.
     * @example '@web/uploads/files'
     * @see \vova07\imperavi\actions\GetAction::$url
     */
    public $urlToFiles = '@web/files';
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (($this->addImage || $this->uploadImage) && !($this->pathToImages && $this->urlToImages)) {
            throw new InvalidConfigException("For add or upload image via Imperavi Redactor you must be set"
                . " 'pathToImages' and 'urlToImages'.");
        } 
        if (($this->addFile || $this->uploadFile) && !($this->pathToFiles && $this->urlToFiles)) {
            throw new InvalidConfigException("For add or upload file via Imperavi Redactor you must be set"
                . " 'pathToFiles' and 'urlToFiles'.");
        } 
    }

    public static function sourceLanguage(){                    
        $local = Yii::$app->sourceLanguage;
        Yii::$app->formatter->locale = strtolower(substr($local, 0, 2)).'_'.strtoupper(substr($local, 0, 2));        
        return strtolower(substr($local, 0, 2));
    }

    /**
     * Font-Awesome icon css class
     * @return string
     */
    public static function getIcon(){
        return 'fa-shopping-basket';
    }

    /**
     * Translates a message to the specified language.
     * 
     * @param string $message the message to be translated.
     * @param array $params the parameters that will be used to replace the corresponding placeholders in the message.
     * @param string $language the language code (e.g. `en-US`, `en`). If this is null, the current application language
     * will be used.
     * @return string
     */
    public static function t($message, $params = [], $language = null)
    {
        return Yii::t('goods/core', $message, $params, $language);
    }
    
}
