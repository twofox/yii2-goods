<?php

namespace twofox\goods\models;

use Yii;
use twofox\goods\Module;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use creocoder\translateable\TranslateableBehavior;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "{{%goods}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property integer $published
 * @property string $content
 * @property string $title_browser
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $created_at
 * @property string $updated_at
 * 
 * @author Belosludcev Vasilij <bupy765@gmail.com>
 * @since 1.0.0
 */
class Goods extends ActiveRecord
{
    
    /**
     * Value of 'published' field where page is not published.
     */
    const PUBLISHED_NO = 0;
    /**
     * Value of 'published' field where page is published.
     */
    const PUBLISHED_YES = 1;
    
    /**
     * @inheritdoc
     */
     
    public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id;
    }
     
    
    public static function getStatusArray()
    {
        return [
            self::PUBLISHED_NO => Yii::t('goods/core', 'NOT ACTIVE'),
            self::PUBLISHED_YES => Yii::t('goods/core', 'ACTIVE')
        ];
    } 
     
     
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['title', 'height'],
                'slugAttribute' => 'slug',
                'immutable' => true,
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['content', 'meta_title', 'meta_keywords', 'meta_description'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
            'galleryBehavior' => [
             'class' => GalleryBehavior::className(),
             'type' => self::tableName(),
             'extension' => 'jpg',
             'directory' => Yii::getAlias(Yii::$app->getModule('goods')->pathToImages),
             'url' => Yii::getAlias(Yii::$app->getModule('goods')->urlToImages),
             'versions' => [
                 'small' => function ($img) {
                     /** @var \Imagine\Image\ImageInterface $img */
                     return $img
                         ->copy()
                         ->thumbnail(new \Imagine\Image\Box(200, 200));
                 },
                 'medium' => function ($img) {
                     /** @var Imagine\Image\ImageInterface $img */
                     $dstSize = $img->getSize();
                     $maxWidth = 800;
                     if ($dstSize->getWidth() > $maxWidth) {
                         $dstSize = $dstSize->widen($maxWidth);
                     }
                     return $img
                         ->copy()
                         ->resize($dstSize);
                 },
             ]
         ]
        ];
    }


    static function getGoodsArray(){
        $data = self::find()
                ->asArray()
                ->select(['`goods`.`id`', 'goods.localization', new \yii\db\Expression('CONCAT_WS(\', \', `goods`.`title`, `goods-localization`.`title`) as title')])
                ->joinWith(['local'])
                ->where(['goods.published' => self::PUBLISHED_YES])
                ->orderBy('`goods`.`title` ASC');
                
        $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());    
        if(!isset($roles['admin']) && !isset($roles['director'])){
            //$query->andFilterWhere(['timetable.status'=>Timetable::PUBLISHED_YES]);
            $data->andFilterWhere(['like', 'goods.managers', '"'.Yii::$app->user->getId().'"']);
        }
            
        return ArrayHelper::map(
            $data->all(),
            'id',
            'title');
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Yii::$app->getModule('goods')->tableName;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'localization', 'price', 'currency', 'height', 'cart_description'], 'required'],
            [['published', 'on_index'], 'boolean'],
            [['annonce'], 'string', 'max' => 1000],
            [['content'], 'string', 'max' => 65535],
            [['title', 'slug', 'cart_description'], 'string', 'max' => 255],
            [['meta_keywords', 'meta_description', 'meta_title'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['localization'], 'integer'],
            ['localization', 'exist', 'targetClass' => 'twofox\goods\models\GoodsLocalization', 'targetAttribute' => 'id'],
            [['title'], 'trim'],
            [['price', 'products', 'currency', 'height', 'managers'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('goods/core', 'ID'),
            'title' => Yii::t('goods/core', 'TITLE'),
            'slug' => Yii::t('goods/core', 'Slug'),
            'annonce' => Yii::t('goods/core', 'Description'),
            'published' => Yii::t('goods/core', 'PUBLISHED'),
            'content' => Yii::t('goods/core', 'CONTENT'),
            'meta_title' => Yii::t('goods/core', 'META TITLE'),
            'meta_keywords' => Yii::t('goods/core', 'META KEYWORDS'),
            'meta_description' => Yii::t('goods/core', 'META KEYWORDS'),
            'created_at' => Yii::t('goods/core', 'CREATED AT'),
            'updated_at' => Yii::t('goods/core', 'UPDATED AT'),
            'price' => Yii::t('goods/core', 'Price'),
            'products' => Yii::t('goods/core', 'Products'),
            'on_index' => Yii::t('goods/core', 'On main page'),
            'height' => Yii::t('goods/core', 'Height, m'),
            'cart_description' => Yii::t('goods/core', 'Cart Description'),
            'currency' => Yii::t('goods/core', 'Currency'),
            'localization' => Yii::t('goods/core', 'Localization'),
            'managers' => Yii::t('goods/core', 'Managers'),
        ];
    }
    
    /**
     * List values of field 'published' with label.
     * @return array
     */
    static public function publishedDropDownList()
    {
        $formatter = Yii::$app->formatter;
        return [
            self::PUBLISHED_YES => $formatter->asBoolean(self::PUBLISHED_YES), 
            self::PUBLISHED_NO => $formatter->asBoolean(self::PUBLISHED_NO),       
        ];
    }
    
    /**
     * List values of field 'published' with label.
     * @return array
     */
    static public function onindexDropDownList()
    {
        $formatter = Yii::$app->formatter;
        return [ 
            self::PUBLISHED_NO => $formatter->asBoolean(self::PUBLISHED_NO),   
            self::PUBLISHED_YES => $formatter->asBoolean(self::PUBLISHED_YES),    
        ];
    }
    
    
    public function beforeSave($insert){
       if (parent::beforeSave($insert)) {
        $this->products = is_array($this->products) ? json_encode($this->products) : '[]'; 
        $this->managers = is_array($this->managers) ? json_encode($this->managers) : '[]'; 
        return true;
       }else
        return false;
    }

    public function afterFind(){
        parent::afterFind();
        $this->products = json_decode($this->products, 1);
        $this->managers = json_decode($this->managers, 1); 
    }    
       
    
    public function transactions(){
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    
    public function getTranslations(){
        return $this->hasMany(PostTranslation::className(), ['post_id' => 'id'])->where(['class' => self::className()]);
    }
    
        
    
    public function getProd($products = [], $published=true){    
        $productsq = GoodsProducts::find()->where(['and', ['in', 'id', (count($products) > 0 ? $products : $this->products)]])->indexBy('id');
                
        if($published==true)
        $productsq->andFilterWhere(['published'=>GoodsProducts::PUBLISHED_YES]);
        
        return $productsq->all();
    }    
    
    public function getLocal(){
        return $this->hasOne(GoodsLocalization::className(), ['id' => 'localization']);
    } 
    
    public function getAllLocalizations(){
        $data = GoodsLocalization::find()->where(['published'=>GoodsLocalization::PUBLISHED_YES])->orderBy('town ASC')->all();        
        
        $return = [];
        foreach (GoodsLocalization::getGoodsLocalizationObject() as $key => $value) {
           $return[] = ['title' => ($value->hasTranslation() ? $value->translate()->town : $value->town), 'id'=>$value->id];
        }
        return $return;
    }
    
    
    static public function getAllProducts($product = null, $currency = null, $q = null, $adv = null, $new=1){
        $data = GoodsProducts::find()->where(['published'=>GoodsProducts::PUBLISHED_YES]);  
                        
        if($product!==null)
        $data->andWhere(['id'=>$product]);  

        if($new==1)
        $data->andWhere(['remove'=>0]);   
        
        if($adv!==null && $adv>0)    {
           $adv = self::findOne($adv);
           $data->andWhere(['in', 'id', $adv->products]);   
        }
        
        if($currency!==null)
        $data->andWhere(['currency'=>$currency]); 
        
        if($q!==null)
        $data->andWhere(['like', 'title', strip_tags($q)]);
        
        return ArrayHelper::toArray($data->all());
    }
    
    
    static public function getAllProductsObject($product = null, $currency = null, $q = null, $adv = null, $new=1){
        $data = GoodsProducts::find()->where(['published'=>GoodsProducts::PUBLISHED_YES])->indexBy('id');  
                        
        if($product!==null)
        $data->andWhere(['id'=>$product]);  

        if($new==1)
        $data->andWhere(['remove'=>0]);   
        
        if($adv!==null && $adv>0)    {
           $adv = self::findOne($adv);
           $data->andWhere(['in', 'id', $adv->products]);   
        }
        
        if($currency!==null)
        $data->andWhere(['currency'=>$currency]); 
        
        if($q!==null)
        $data->andWhere(['like', 'title', strip_tags($q)]);
        
        return $data->all();
    }
    
    

    /**
     * Formats all model errors into a single string
     * @return string
     */
    public function formatErrors()
    {
        $result = '';
        foreach($this->getErrors() as $attribute => $errors) {
            $result .= implode(" ", $errors)." ";
        }
        return $result;
    }
    
}
