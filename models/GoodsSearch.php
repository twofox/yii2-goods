<?php

namespace twofox\goods\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use twofox\goods\models\Goods;

/**
 * goodsearch represents the model behind the search form about `twofox\goods\models\Page`.
 * 
 * @author Belosludcev Vasilij <bupy765@gmail.com>
 * @since 1.0.0
 */
class GoodsSearch extends Goods
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'published', 'localization'], 'integer'],
            [['title', 'slug', 'created_at', 'updated_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = parent::find()->where(['remove'=>0]);
        $dataProvider = new ActiveDataProvider(['query' => $query,]);
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'published' => $this->published,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'localization' => $this->localization,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug]);            
            
        return $dataProvider;
    }    
}
