<?php

namespace twofox\goods\models;

use Yii;
use twofox\goods\Module;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use creocoder\translateable\TranslateableBehavior;
use zxbodya\yii2\galleryManager\GalleryBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%goods}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property integer $published
 * @property string $content
 * @property string $title_browser
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $created_at
 * @property string $updated_at
 * 
 * @author Belosludcev Vasilij <bupy765@gmail.com>
 * @since 1.0.0
 */
class GoodsLocalization extends ActiveRecord
{
    
    /**
     * Value of 'published' field where page is not published.
     */
    const PUBLISHED_NO = 0;
    /**
     * Value of 'published' field where page is published.
     */
    const PUBLISHED_YES = 1;
    
    /**
     * @inheritdoc
     */
     
    
    public static function getStatusArray()
    {
        return [
            self::PUBLISHED_NO => Module::t('NOT ACTIVE'),
            self::PUBLISHED_YES => Module::t('ACTIVE')
        ];
    } 
     
     
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => ['country', 'town'],
                'slugAttribute' => 'slug',
                'immutable' => true,
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['content', 'address', 'annonce'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
            'galleryBehavior' => [
            'class' => GalleryBehavior::className(),
            'type' => self::tableName(),
            'extension' => 'jpg',
            'directory' => Yii::getAlias(Yii::$app->getModule('goods')->pathToImages),
            'url' => Yii::getAlias(Yii::$app->getModule('goods')->urlToImages),
            'versions' => [
                 'small' => function ($img) {
                     /** @var \Imagine\Image\ImageInterface $img */
                     return $img
                         ->copy()
                         ->thumbnail(new \Imagine\Image\Box(200, 200));
                 },
                 'medium' => function ($img) {
                     /** @var Imagine\Image\ImageInterface $img */
                     $dstSize = $img->getSize();
                     $maxWidth = 800;
                     if ($dstSize->getWidth() > $maxWidth) {
                         $dstSize = $dstSize->widen($maxWidth);
                     }
                     return $img
                         ->copy()
                         ->resize($dstSize);
                 },
              ]
            ]
        ];
    }


    static function getGoodsLocalizationArray(){
        return ArrayHelper::map(
            ArrayHelper::toArray(
                self::find()
                ->where(['published' => self::PUBLISHED_YES])
                ->orderBy('town ASC')
                ->all()
            ),
            'id',           
            'title');
    }
    
    


    static function getGoodsLocalizationObject(){
        return self::find()
                ->where(['published' => self::PUBLISHED_YES])
                ->orderBy('town ASC')
                ->all();
    }    
    
    public function beforeSave($insert){
       if (parent::beforeSave($insert)) {
        $this->title = $this->country.' - '.$this->town; 
        return true;
       }else
        return false;
    }
    
    public function afterFind(){
        parent::afterFind();
        $this->title = $this->town . ' ('.$this->country.')';
    }  
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Yii::$app->getModule('goods')->tableNameLocalization;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address', 'country', 'town'], 'required'],
            [['published'], 'boolean'],
            [['annonce', 'country'], 'string', 'max' => 1000],
            [['content'], 'string', 'max' => 65535],
            [['title', 'slug', 'address', 'town'], 'string', 'max' => 255],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('goods/core', 'ID'),
            'title' => Yii::t('goods/core', 'TITLE'),
            'slug' => Yii::t('goods/core', 'Slug'),
            'annonce' => Yii::t('goods/core', 'Description'),
            'published' => Yii::t('goods/core', 'PUBLISHED'),
            'content' => Yii::t('goods/core', 'CONTENT'),
            'meta_title' => Yii::t('goods/core', 'META TITLE'),
            'meta_keywords' => Yii::t('goods/core', 'META KEYWORDS'),
            'meta_description' => Yii::t('goods/core', 'META KEYWORDS'),
            'created_at' => Yii::t('goods/core', 'CREATED AT'),
            'updated_at' => Yii::t('goods/core', 'UPDATED AT'),
            'address' => Yii::t('goods/core', 'Geographical coordinates'),
            'town' => Yii::t('goods/core', 'Town'),
        ];
    }    
    
    /**
     * List values of field 'published' with label.
     * @return array
     */
    static public function publishedDropDownList()
    {
        $formatter = Yii::$app->formatter;
        return [
            self::PUBLISHED_YES => $formatter->asBoolean(self::PUBLISHED_YES), 
            self::PUBLISHED_NO => $formatter->asBoolean(self::PUBLISHED_NO),       
        ];
    }
    
    public function transactions(){
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    
    public function getTranslations(){
        return $this->hasMany(PostTranslation::className(), ['post_id' => 'id'])->where(['class' => self::className()]);
    }    

    /**
     * Formats all model errors into a single string
     * @return string
     */
    public function formatErrors()
    {
        $result = '';
        foreach($this->getErrors() as $attribute => $errors) {
            $result .= implode(" ", $errors)." ";
        }
        return $result;
    }
    
    public function getCountryName() {
        return self::getCountryArray()[$this->country];
    }
    
    static public function getCountryArray() {
        return ["AF" => Yii::t('country', "Afghanistan (‫افغانستان‬‎)"), "AX" => Yii::t('country', "Åland Islands (Åland)"), "AL" => Yii::t('country', "Albania (Shqipëri)"), "DZ" => Yii::t('country', "Algeria"), "AS" => Yii::t('country', "American Samoa"), "AD" => Yii::t('country', "Andorra"), "AO" => Yii::t('country', "Angola"), "AI" => Yii::t('country', "Anguilla"), "AQ" => Yii::t('country', "Antarctica"), "AG" => Yii::t('country', "Antigua &amp; Barbuda"), "AR" => Yii::t('country', "Argentina"), "AM" => Yii::t('country', "Armenia (Հայաստան)"), "AW" => Yii::t('country', "Aruba"), "AC" => Yii::t('country', "Ascension Island"), "AU" => Yii::t('country', "Australia"), "AT" => Yii::t('country', "Austria (Österreich)"), "AZ" => Yii::t('country', "Azerbaijan (Azərbaycan)"), "BS" => Yii::t('country', "Bahamas"), "BH" => Yii::t('country', "Bahrain (‫البحرين‬‎)"), "BD" => Yii::t('country', "Bangladesh (বাংলাদেশ)"), "BB" => Yii::t('country', "Barbados"), "BY" => Yii::t('country', "Belarus (Беларусь)"), "BE" => Yii::t('country', "Belgium"), "BZ" => Yii::t('country', "Belize"), "BJ" => Yii::t('country', "Benin (Bénin)"), "BM" => Yii::t('country', "Bermuda"), "BT" => Yii::t('country', "Bhutan (འབྲུག)"), "BO" => Yii::t('country', "Bolivia"), "BA" => Yii::t('country', "Bosnia &amp; Herzegovina (Босна и Херцеговина)"), "BW" => Yii::t('country', "Botswana"), "BV" => Yii::t('country', "Bouvet Island"), "BR" => Yii::t('country', "Brazil (Brasil)"), "IO" => Yii::t('country', "British Indian Ocean Territory"), "VG" => Yii::t('country', "British Virgin Islands"), "BN" => Yii::t('country', "Brunei"), "BG" => Yii::t('country', "Bulgaria (България)"), "BF" => Yii::t('country', "Burkina Faso"), "BI" => Yii::t('country', "Burundi (Uburundi)"), "KH" => Yii::t('country', "Cambodia (កម្ពុជា)"), "CM" => Yii::t('country', "Cameroon (Cameroun)"), "CA" => Yii::t('country', "Canada"), "IC" => Yii::t('country', "Canary Islands (Canarias)"), "CV" => Yii::t('country', "Cape Verde (Kabu Verdi)"), "BQ" => Yii::t('country', "Caribbean Netherlands"), "KY" => Yii::t('country', "Cayman Islands"), "CF" => Yii::t('country', "Central African Republic (République centrafricaine)"), "EA" => Yii::t('country', "Ceuta &amp; Melilla (Ceuta y Melilla)"), "TD" => Yii::t('country', "Chad (Tchad)"), "CL" => Yii::t('country', "Chile"), "CN" => Yii::t('country', "China (中国)"), "CX" => Yii::t('country', "Christmas Island"), "CP" => Yii::t('country', "Clipperton Island"), "CC" => Yii::t('country', "Cocos (Keeling) Islands (Kepulauan Cocos (Keeling))"), "CO" => Yii::t('country', "Colombia"), "KM" => Yii::t('country', "Comoros (‫جزر القمر‬‎)"), "CD" => Yii::t('country', "Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)"), "CG" => Yii::t('country', "Congo (Republic) (Congo-Brazzaville)"), "CK" => Yii::t('country', "Cook Islands"), "CR" => Yii::t('country', "Costa Rica"), "CI" => Yii::t('country', "Côte d’Ivoire"), "HR" => Yii::t('country', "Croatia (Hrvatska)"), "CU" => Yii::t('country', "Cuba"), "CW" => Yii::t('country', "Curaçao"), "CY" => Yii::t('country', "Cyprus (Κύπρος)"), "CZ" => Yii::t('country', "Czech Republic (Česká republika)"), "DK" => Yii::t('country', "Denmark (Danmark)"), "DG" => Yii::t('country', "Diego Garcia"), "DJ" => Yii::t('country', "Djibouti"), "DM" => Yii::t('country', "Dominica"), "DO" => Yii::t('country', "Dominican Republic (República Dominicana)"), "EC" => Yii::t('country', "Ecuador"), "EG" => Yii::t('country', "Egypt (‫مصر‬‎)"), "SV" => Yii::t('country', "El Salvador"), "GQ" => Yii::t('country', "Equatorial Guinea (Guinea Ecuatorial)"), "ER" => Yii::t('country', "Eritrea"), "EE" => Yii::t('country', "Estonia (Eesti)"), "ET" => Yii::t('country', "Ethiopia"), "FK" => Yii::t('country', "Falkland Islands (Islas Malvinas)"), "FO" => Yii::t('country', "Faroe Islands (Føroyar)"), "FJ" => Yii::t('country', "Fiji"), "FI" => Yii::t('country', "Finland (Suomi)"), "FR" => Yii::t('country', "France"), "GF" => Yii::t('country', "French Guiana (Guyane française)"), "PF" => Yii::t('country', "French Polynesia (Polynésie française)"), "TF" => Yii::t('country', "French Southern Territories (Terres australes françaises)"), "GA" => Yii::t('country', "Gabon"), "GM" => Yii::t('country', "Gambia"), "GE" => Yii::t('country', "Georgia (საქართველო)"), "DE" => Yii::t('country', "Germany (Deutschland)"), "GH" => Yii::t('country', "Ghana (Gaana)"), "GI" => Yii::t('country', "Gibraltar"), "GR" => Yii::t('country', "Greece (Ελλάδα)"), "GL" => Yii::t('country', "Greenland (Kalaallit Nunaat)"), "GD" => Yii::t('country', "Grenada"), "GP" => Yii::t('country', "Guadeloupe"), "GU" => Yii::t('country', "Guam"), "GT" => Yii::t('country', "Guatemala"), "GG" => Yii::t('country', "Guernsey"), "GN" => Yii::t('country', "Guinea (Guinée)"), "GW" => Yii::t('country', "Guinea-Bissau (Guiné-Bissau)"), "GY" => Yii::t('country', "Guyana"), "HT" => Yii::t('country', "Haiti"), "HM" => Yii::t('country', "Heard &amp; McDonald Islands"), "HN" => Yii::t('country', "Honduras"), "HK" => Yii::t('country', "Hong Kong (香港)"), "HU" => Yii::t('country', "Hungary (Magyarország)"), "IS" => Yii::t('country', "Iceland (Ísland)"), "IN" => Yii::t('country', "India (भारत)"), "ID" => Yii::t('country', "Indonesia"), "IR" => Yii::t('country', "Iran (‫ایران‬‎)"), "IQ" => Yii::t('country', "Iraq (‫العراق‬‎)"), "IE" => Yii::t('country', "Ireland"), "IM" => Yii::t('country', "Isle of Man"), "IL" => Yii::t('country', "Israel (‫ישראל‬‎)"), "IT" => Yii::t('country', "Italy (Italia)"), "JM" => Yii::t('country', "Jamaica"), "JP" => Yii::t('country', "Japan (日本)"), "JE" => Yii::t('country', "Jersey"), "JO" => Yii::t('country', "Jordan (‫الأردن‬‎)"), "KZ" => Yii::t('country', "Kazakhstan (Казахстан)"), "KE" => Yii::t('country', "Kenya"), "KI" => Yii::t('country', "Kiribati"), "XK" => Yii::t('country', "Kosovo (Kosovë)"), "KW" => Yii::t('country', "Kuwait (‫الكويت‬‎)"), "KG" => Yii::t('country', "Kyrgyzstan (Кыргызстан)"), "LA" => Yii::t('country', "Laos (ລາວ)"), "LV" => Yii::t('country', "Latvia (Latvija)"), "LB" => Yii::t('country', "Lebanon (‫لبنان‬‎)"), "LS" => Yii::t('country', "Lesotho"), "LR" => Yii::t('country', "Liberia"), "LY" => Yii::t('country', "Libya (‫ليبيا‬‎)"), "LI" => Yii::t('country', "Liechtenstein"), "LT" => Yii::t('country', "Lithuania (Lietuva)"), "LU" => Yii::t('country', "Luxembourg"), "MO" => Yii::t('country', "Macau (澳門)"), "MK" => Yii::t('country', "Macedonia (FYROM) (Македонија)"), "MG" => Yii::t('country', "Madagascar (Madagasikara)"), "MW" => Yii::t('country', "Malawi"), "MY" => Yii::t('country', "Malaysia"), "MV" => Yii::t('country', "Maldives"), "ML" => Yii::t('country', "Mali"), "MT" => Yii::t('country', "Malta"), "MH" => Yii::t('country', "Marshall Islands"), "MQ" => Yii::t('country', "Martinique"), "MR" => Yii::t('country', "Mauritania (‫موريتانيا‬‎)"), "MU" => Yii::t('country', "Mauritius (Moris)"), "YT" => Yii::t('country', "Mayotte"), "MX" => Yii::t('country', "Mexico (México)"), "FM" => Yii::t('country', "Micronesia"), "MD" => Yii::t('country', "Moldova (Republica Moldova)"), "MC" => Yii::t('country', "Monaco"), "MN" => Yii::t('country', "Mongolia (Монгол)"), "ME" => Yii::t('country', "Montenegro (Crna Gora)"), "MS" => Yii::t('country', "Montserrat"), "MA" => Yii::t('country', "Morocco"), "MZ" => Yii::t('country', "Mozambique (Moçambique)"), "MM" => Yii::t('country', "Myanmar (Burma) (မြန်မာ)"), "NA" => Yii::t('country', "Namibia (Namibië)"), "NR" => Yii::t('country', "Nauru"), "NP" => Yii::t('country', "Nepal (नेपाल)"), "NL" => Yii::t('country', "Netherlands (Nederland)"), "NC" => Yii::t('country', "New Caledonia (Nouvelle-Calédonie)"), "NZ" => Yii::t('country', "New Zealand"), "NI" => Yii::t('country', "Nicaragua"), "NE" => Yii::t('country', "Niger (Nijar)"), "NG" => Yii::t('country', "Nigeria"), "NU" => Yii::t('country', "Niue"), "NF" => Yii::t('country', "Norfolk Island"), "MP" => Yii::t('country', "Northern Mariana Islands"), "KP" => Yii::t('country', "North Korea (조선민주주의인민공화국)"), "NO" => Yii::t('country', "Norway (Norge)"), "OM" => Yii::t('country', "Oman (‫عُمان‬‎)"), "PK" => Yii::t('country', "Pakistan (‫پاکستان‬‎)"), "PW" => Yii::t('country', "Palau"), "PS" => Yii::t('country', "Palestine (‫فلسطين‬‎)"), "PA" => Yii::t('country', "Panama (Panamá)"), "PG" => Yii::t('country', "Papua New Guinea"), "PY" => Yii::t('country', "Paraguay"), "PE" => Yii::t('country', "Peru (Perú)"), "PH" => Yii::t('country', "Philippines"), "PN" => Yii::t('country', "Pitcairn Islands"), "PL" => Yii::t('country', "Poland (Polska)"), "PT" => Yii::t('country', "Portugal"), "PR" => Yii::t('country', "Puerto Rico"), "QA" => Yii::t('country', "Qatar (‫قطر‬‎)"), "RE" => Yii::t('country', "Réunion (La Réunion)"), "RO" => Yii::t('country', "Romania (România)"), "RU" => Yii::t('country', "Russia (Россия)"), "RW" => Yii::t('country', "Rwanda"), "WS" => Yii::t('country', "Samoa"), "SM" => Yii::t('country', "San Marino"), "ST" => Yii::t('country', "São Tomé &amp; Príncipe (São Tomé e Príncipe)"), "SA" => Yii::t('country', "Saudi Arabia (‫المملكة العربية السعودية‬‎)"), "SN" => Yii::t('country', "Senegal"), "RS" => Yii::t('country', "Serbia (Србија)"), "SC" => Yii::t('country', "Seychelles"), "SL" => Yii::t('country', "Sierra Leone"), "SG" => Yii::t('country', "Singapore"), "SX" => Yii::t('country', "Sint Maarten"), "SK" => Yii::t('country', "Slovakia (Slovensko)"), "SI" => Yii::t('country', "Slovenia (Slovenija)"), "SB" => Yii::t('country', "Solomon Islands"), "SO" => Yii::t('country', "Somalia (Soomaaliya)"), "ZA" => Yii::t('country', "South Africa"), "GS" => Yii::t('country', "South Georgia &amp; South Sandwich Islands"), "KR" => Yii::t('country', "South Korea (대한민국)"), "SS" => Yii::t('country', "South Sudan (‫جنوب السودان‬‎)"), "ES" => Yii::t('country', "Spain (España)"), "LK" => Yii::t('country', "Sri Lanka (ශ්‍රී ලංකාව)"), "BL" => Yii::t('country', "St. Barthélemy (Saint-Barthélemy)"), "SH" => Yii::t('country', "St. Helena"), "KN" => Yii::t('country', "St. Kitts &amp; Nevis"), "LC" => Yii::t('country', "St. Lucia"), "MF" => Yii::t('country', "St. Martin (Saint-Martin)"), "PM" => Yii::t('country', "St. Pierre &amp; Miquelon (Saint-Pierre-et-Miquelon)"), "VC" => Yii::t('country', "St. Vincent &amp; Grenadines"), "SD" => Yii::t('country', "Sudan (‫السودان‬‎)"), "SR" => Yii::t('country', "Suriname"), "SJ" => Yii::t('country', "Svalbard &amp; Jan Mayen (Svalbard og Jan Mayen)"), "SZ" => Yii::t('country', "Swaziland"), "SE" => Yii::t('country', "Sweden (Sverige)"), "CH" => Yii::t('country', "Switzerland (Schweiz)"), "SY" => Yii::t('country', "Syria (‫سوريا‬‎)"), "TW" => Yii::t('country', "Taiwan (台灣)"), "TJ" => Yii::t('country', "Tajikistan"), "TZ" => Yii::t('country', "Tanzania"), "TH" => Yii::t('country', "Thailand (ไทย)"), "TL" => Yii::t('country', "Timor-Leste"), "TG" => Yii::t('country', "Togo"), "TK" => Yii::t('country', "Tokelau"), "TO" => Yii::t('country', "Tonga"), "TT" => Yii::t('country', "Trinidad &amp; Tobago"), "TA" => Yii::t('country', "Tristan da Cunha"), "TN" => Yii::t('country', "Tunisia"), "TR" => Yii::t('country', "Turkey (Türkiye)"), "TM" => Yii::t('country', "Turkmenistan"), "TC" => Yii::t('country', "Turks &amp; Caicos Islands"), "TV" => Yii::t('country', "Tuvalu"), "UM" => Yii::t('country', "U.S. Outlying Islands"), "VI" => Yii::t('country', "U.S. Virgin Islands"), "UG" => Yii::t('country', "Uganda"), "UA" => Yii::t('country', "Ukraine (Україна)"), "AE" => Yii::t('country', "United Arab Emirates (‫الإمارات العربية المتحدة‬‎)"), "GB" => Yii::t('country', "United Kingdom"), "US" => Yii::t('country', "United States"), "UY" => Yii::t('country', "Uruguay"), "UZ" => Yii::t('country', "Uzbekistan (Oʻzbekiston)"), "VU" => Yii::t('country', "Vanuatu"), "VA" => Yii::t('country', "Vatican City (Città del Vaticano)"), "VE" => Yii::t('country', "Venezuela"), "VN" => Yii::t('country', "Vietnam (Việt Nam)"), "WF" => Yii::t('country', "Wallis &amp; Futuna"), "EH" => Yii::t('country', "Western Sahara (‫الصحراء الغربية‬‎)"), "YE" => Yii::t('country', "Yemen (‫اليمن‬‎)"), "ZM" => Yii::t('country', "Zambia"), "ZW" => Yii::t('country', "Zimbabwe")];
    }
}
