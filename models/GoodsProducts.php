<?php

namespace twofox\goods\models;

use Yii;
use twofox\goods\Module;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use creocoder\translateable\TranslateableBehavior;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "{{%goods}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property integer $published
 * @property string $content
 * @property string $title_browser
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $created_at
 * @property string $updated_at
 * 
 * @author Belosludcev Vasilij <bupy765@gmail.com>
 * @since 1.0.0
 */
class GoodsProducts extends ActiveRecord
{
    
    /**
     * Value of 'published' field where page is not published.
     */
    const PUBLISHED_NO = 0;
    /**
     * Value of 'published' field where page is published.
     */
    const PUBLISHED_YES = 1;
    
    /**
     * @inheritdoc
     */
     
    public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id;
    }
     
    
    public static function getStatusArray()
    {
        return [
            self::PUBLISHED_NO => Yii::t('goods/core', 'NOT ACTIVE'),
            self::PUBLISHED_YES => Yii::t('goods/core', 'ACTIVE')
        ];
    } 
     
     
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'immutable' => true,
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['content', 'meta_title', 'meta_keywords', 'meta_description', 'cart_description'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
            'galleryBehavior' => [
             'class' => GalleryBehavior::className(),
             'type' => self::tableName(),
             'extension' => 'jpg',
             'directory' => Yii::getAlias(Yii::$app->getModule('goods')->pathToImages),
             'url' => Yii::getAlias(Yii::$app->getModule('goods')->urlToImages),
             'versions' => [
                 'small' => function ($img) {
                     /** @var \Imagine\Image\ImageInterface $img */
                     return $img
                         ->copy()
                         ->thumbnail(new \Imagine\Image\Box(200, 200));
                 },
                 'medium' => function ($img) {
                     /** @var Imagine\Image\ImageInterface $img */
                     $dstSize = $img->getSize();
                     $maxWidth = 800;
                     if ($dstSize->getWidth() > $maxWidth) {
                         $dstSize = $dstSize->widen($maxWidth);
                     }
                     return $img
                         ->copy()
                         ->resize($dstSize);
                 },
              ]
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Yii::$app->getModule('goods')->tableNameProducts;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'price', 'currency', 'cart_description'], 'required'],
            [['published'], 'boolean'],
            [['annonce'], 'string', 'max' => 1000],
            [['content'], 'string', 'max' => 65535],
            [['title', 'slug', 'cart_description', 'meta_keywords', 'meta_description', 'meta_title'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['price', 'currency'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('goods/core', 'ID'),
            'title' => Yii::t('goods/core', 'TITLE'),
            'slug' => Yii::t('goods/core', 'Slug'),
            'annonce' => Yii::t('goods/core', 'Description'),
            'published' => Yii::t('goods/core', 'PUBLISHED'),
            'content' => Yii::t('goods/core', 'CONTENT'),
            'meta_title' => Yii::t('goods/core', 'META TITLE'),
            'meta_keywords' => Yii::t('goods/core', 'META KEYWORDS'),
            'meta_description' => Yii::t('goods/core', 'META KEYWORDS'),
            'created_at' => Yii::t('goods/core', 'CREATED AT'),
            'updated_at' => Yii::t('goods/core', 'UPDATED AT'),
            'price' => Yii::t('goods/core', 'Price'),
            'cart_description' => Yii::t('goods/core', 'Cart Description'),
        ];
    }    
    
    /**
     * List values of field 'published' with label.
     * @return array
     */
    static public function publishedDropDownList()
    {
        $formatter = Yii::$app->formatter;
        return [
            self::PUBLISHED_YES => $formatter->asBoolean(self::PUBLISHED_YES),   
            self::PUBLISHED_NO => $formatter->asBoolean(self::PUBLISHED_NO),     
        ];
    }
    
    public function transactions(){
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    
    public function getTranslations(){
        return $this->hasMany(PostTranslation::className(), ['post_id' => 'id'])->where(['class' => self::className()]);
    }    

    /**
     * Formats all model errors into a single string
     * @return string
     */
    public function formatErrors()
    {
        $result = '';
        foreach($this->getErrors() as $attribute => $errors) {
            $result .= implode(" ", $errors)." ";
        }
        return $result;
    }
    
}
